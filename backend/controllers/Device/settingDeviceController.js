import { devices } from "../../models/DeviceModel.js";
import { storeOrders } from "../Payment/ordersController.js";
import crypto from "crypto";

export const showDevice = async(req, res) => {
    try {
        const show_devices = await devices.findAll({
            order: [
                ['createdAt', 'DESC']
            ]
        });
        res.json(show_devices);
    } catch (error) {
        console.log(error);
    }
}

export const getDevice = async(req, res) => {
    const { device_id } = req.params
    try {
        const device = await devices.findOne({
            where: {
                device_id: device_id
            }
        });

        res.json(device);
    } catch (error) {
        console.log(error);
    }
}

export const storeDevice = async(req, res) => {
    try {
        const device_id = Math.floor(Math.random() * 1000)+101
        const api_key = await generateApiKey(device_id)
        const telp = await pregTelp(req.body.telp)
        
        const store_devices = await devices.create({
            device_id: device_id,
            name: req.body.name,
            telp: telp,
            user_id: req.body.user_id
        })

        const store_orders = await storeOrders(device_id, req.body.product_id)
        res.json(store_orders);
    } catch (error) {
        console.log(error);
    }
}

export const updateDevice = async(device_id, updates) => {
    try {
        const update_deivce = await devices.update(updates,
            { where: { device_id: device_id } }
        )
        
        return 1;
    } catch (error) {
        console.log(error);
    }
}

export const updateWebhook = async(req, res) => {
    try {
        const param_update_device =  {
            webhook: req.body.webhook
        }

        const update_device = await updateDevice(req.body.device_id, param_update_device)
        
        res.json(req.body.webhook);
    } catch (error) {
        console.log(error);
    }
}

export const generateApiKey = async(device_id) => {
    try {
        const random = crypto.randomBytes(8)
                .toString('hex');
        const api_key = 'ECOM.'+random+device_id
        return api_key
    } catch (error) {
        console.log(error);
    }
}

export const pregTelp = async(telp) => {
    var telp = telp.replace('+', "")
    var telp = telp.replace(/[^0-9]/, "")
    var telp = telp.replace(/^\+?0|\D/, "62")
    var telp = telp.replace(/^\+?8|\|8|\D/, "628")

    return telp;
}

export const getSessionId = async(api_key) => {
    const device = await devices.findOne({
        where: {
            api_key: api_key
        }
    });
    
    return device ?? null 
}
