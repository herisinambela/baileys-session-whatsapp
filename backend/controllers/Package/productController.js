import { products } from "../../models/PackageModel.js";

export const showProduct = async(req, res) => {
    try {
        const show_products = await products.findAll();
        res.json(show_products);
    } catch (error) {
        console.log(error);
    }
}

export const getProduct = async(product_id) => {
    try {
        const get_product = await products.findOne({ 
            where: { product_id: product_id } 
        });
        return get_product
    } catch (error) {
        console.log(error);
    }
}