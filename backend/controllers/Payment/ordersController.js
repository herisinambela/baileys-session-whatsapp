import date from 'date-and-time';
import { orders, detail_orders } from "../../models/PaymentModel.js";
import { getProduct } from "../Package/productController.js";
import { updateDevice, generateApiKey } from "../Device/settingDeviceController.js";

export const storeOrders = async(device_id, product_id) => {
    try {
        const get_product = await getProduct(product_id)
        const order_id = Math.floor(Math.random() * 10000)+1001

        const current_time = new Date()
        const expired_at = date.addHours(current_time, 3);
        const store_orders = await orders.create({
            order_id: order_id,
            device_id: device_id,
            total_payment: get_product.price,
            payment_status: "UNPAID",
            expired_at: date.format(expired_at, 'YYYY-MM-DD HH:mm:ss')
        })
        const store_detail_orders = await storeDetailOrders(order_id, product_id)
        
        return store_orders
    } catch (error) {
        console.log(error);
    }
}

export const storeDetailOrders = async(order_id, product_id) => {
    try {
        const get_product = await getProduct(product_id)
        const store_detail_orders = await detail_orders.create({
            order_id: order_id,
            product_id: product_id,
            total_payment: get_product.price*1,
            total_product: 1
        })
        
        return 1
    } catch (error) {
        console.log(error);
    }
}

export const updateOrders = async(order_id, updates) => {
    try {
        const update_orders = await orders.update(updates,
            { where: { order_id: order_id } }
        )
        
        return 1
    } catch (error) {
        console.log(error);
    }
}

export const getOrders = async(order_id) => {
    try {
        const get_orders = await orders.findOne({ 
            where: { order_id: order_id } 
        });
        return get_orders
    } catch (error) {
        console.log(error);
    }
}

export const apporoveOrders = async(req, res) => {
    try {
        const get_orders = await getOrders(req.body.order_id)
        const now = new Date();
        const param_update_orders =  {
            order_at: date.format(now, 'YYYY-MM-DD HH:mm:ss'),
            payment_status: "PAID"
        }
        const update_orders = await updateOrders(req.body.order_id, param_update_orders)

        const api_key = await generateApiKey(get_orders.device_id)
        const expired_at = date.addDays(now, 30)
        const param_update_device =  {
            api_key: api_key,
            device_status: 'ACTIVE',
            expired_at: date.format(expired_at, 'YYYY-MM-DD HH:mm:ss')
        }
        const update_device = await updateDevice(get_orders.device_id, param_update_device)

        res.json("success");
    } catch (error) {
        console.log(error);
    }
}

export const showDetailOrders = async(req, res) => {
    const { order_id } = req.params
    try {
        var get_orders = await detail_orders.findAll({ 
            include: [{
                model:orders,
                where: { order_id: order_id }
            }]
        });

        for (let i = 0; i < get_orders.length; i++) {
            var get_product = await getProduct(get_orders[i].product_id)
            get_orders[i].product_id = get_product
        }
        
        res.json(get_orders);
    } catch (error) {
        console.log(error);
    }
}