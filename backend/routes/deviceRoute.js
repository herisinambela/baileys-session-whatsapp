import { Router } from 'express'
import { body } from 'express-validator'
import requestValidator from './../middlewares/requestValidator.js'
import { showDevice, storeDevice, generateApiKey, getDevice, updateWebhook, pregTelp } from '../controllers/Device/settingDeviceController.js'
import uniqueDeviceValidator from './../middlewares/uniqueDeviceValidator.js'
import webhookValidator from './../middlewares/webhookValidator.js'

const router = Router()

router.post('/preg-telp', pregTelp);
router.get('/show-device', showDevice);
router.get('/get-device/:device_id', getDevice);
router.post('/store-device', body('name').notEmpty(), body('telp').notEmpty(), body('product_id').notEmpty(), requestValidator, uniqueDeviceValidator, storeDevice);
router.post('/generate-apikey', generateApiKey);
router.put('/update-webhook', webhookValidator, updateWebhook);

export default router
