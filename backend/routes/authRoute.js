import { Router } from 'express'
import { body } from 'express-validator'
import { verifyToken } from "../middlewares/VerifyToken.js";
import uniqueUsersValidator from './../middlewares/uniqueUsersValidator.js'
import verifyAccount from './../middlewares/verifyAccount.js'
import { getUsers, Register, Login, refreshToken, Logout } from './../controllers/Auth/usersController.js'

const router = Router()

router.get('/users', verifyToken, getUsers);
router.post('/register', uniqueUsersValidator, Register);
router.post('/login', verifyAccount, Login);
router.get('/refresh-token', refreshToken);
router.delete('/logout', Logout);

export default router
