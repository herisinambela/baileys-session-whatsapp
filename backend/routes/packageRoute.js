import { Router } from 'express'
import { showProduct } from '../controllers/Package/productController.js'

const router = Router()

router.get('/show-product', showProduct);

export default router
