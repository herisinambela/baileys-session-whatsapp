import { Router } from 'express'
import { apporoveOrders, showDetailOrders } from '../controllers/Payment/ordersController.js'

const router = Router()

router.post('/approve-orders', apporoveOrders);
router.get('/show-detail-orders/:order_id', showDetailOrders)

export default router
