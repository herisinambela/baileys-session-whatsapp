import { Router } from 'express'
import sessionsRoute from './routes/sessionsRoute.js'
import chatsRoute from './routes/chatsRoute.js'
import groupsRoute from './routes/groupsRoute.js'
import authRoute from './routes/authRoute.js'
import deviceRoute from './routes/deviceRoute.js'
import packageRoute from './routes/packageRoute.js'
import paymentRoute from './routes/paymentRoute.js'
import response from './response.js'

const router = Router()

router.use('/sessions', sessionsRoute)
router.use('/chats', chatsRoute)
router.use('/groups', groupsRoute)
router.use('/auth', authRoute)
router.use('/device', deviceRoute)
router.use('/package', packageRoute)
router.use('/payment', paymentRoute)

router.all('*', (req, res) => {
    response(res, 404, false, 'The requested url cannot be found.')
})

export default router
