import response from './../response.js'
import { users } from "../models/AuthModel.js";

const validate = async (req, res, next) => {
    const device = await users.findOne({
        where: {
            email: req.body.email
        }
    });
    
    if (device) {
        return response(res, 404, false, 'This account has been taken')
    }

    if(req.body.password !== req.body.confirm_password) return response(res, 404, false, 'Confirm password not match');
    
    next()
}

export default validate
