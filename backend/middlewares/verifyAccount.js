import response from './../response.js'
import { users } from "../models/AuthModel.js";
import bcrypt from "bcrypt";

const validate = async (req, res, next) => {
    const user = await users.findAll({
        where:{
            email: req.body.email
        }
    });
    const match = await bcrypt.compare(req.body.password, user[0].password);
    if(!match) return response(res, 404, false, 'Account not match!')
    
    next()
}

export default validate