import { devices } from "../models/DeviceModel.js";
import response from './../response.js'
import { isValidUrl } from './../whatsapp.js'

export const validateAPIKey = async (req, res, next) => {
    const is_valid_url = isValidUrl(req.body.webhook, ['http', 'https'])
    if (!is_valid_url) {
        return response(res, 404, false, 'Endpoint not valid!')
    }

    next();
}

export default validateAPIKey