import { isSessionExists } from '../whatsapp.js'
import { getSessionId } from '../controllers/Device/settingDeviceController.js'
import { devices } from "../models/DeviceModel.js";
import response from './../response.js'

const validate = async(req, res, next) => {
    const apiKey = req.headers['angel-key']
    const content_type  = req.headers['content-type'];

    const account = await devices.findOne({ 
        where: { api_key: apiKey }
    });

    if(account === null || content_type !== "application/json"){
        return response(res, 404, false, {error: {code:403, message:'You not allowed.'}})
    }

    const get_session_id = await getSessionId(apiKey)
    const sessionId = get_session_id.device_id

    if (!isSessionExists(sessionId)) {
        return response(res, 404, false, 'Session not found.')
    }

    res.locals.sessionId = sessionId
    next()
}

export default validate
