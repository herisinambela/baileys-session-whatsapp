import response from './../response.js'
import { devices } from "../models/DeviceModel.js";

const validate = async (req, res, next) => {
    const device = await devices.findOne({
        where: {
            telp: req.body.telp
        }
    });
    
    if (device !== null) {
        return response(res, 404, false, 'There is running device!')
    }
    
    next()
}

export default validate
