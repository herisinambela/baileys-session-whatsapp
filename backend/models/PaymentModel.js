// import sequelize 
import { Sequelize } from "sequelize";
// import connection 
import databasePayment from "../config/databasePayment.js";

const orders = databasePayment.define('orders', {
    order_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    device_id: Sequelize.STRING,
    total_payment: Sequelize.STRING,
    order_at: Sequelize.STRING,
    payment_status: Sequelize.STRING,
    expired_at: Sequelize.STRING
});

const detail_orders = databasePayment.define('detail_orders', {
    order_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    product_id: Sequelize.STRING,
    total_payment: Sequelize.STRING,
    total_product: Sequelize.STRING
});

orders.hasMany(detail_orders, {foreignKey: 'order_id'})
detail_orders.belongsTo(orders, {foreignKey: 'order_id'})

export {
    orders,
    detail_orders
};