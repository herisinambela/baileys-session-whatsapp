// import sequelize 
import { Sequelize } from "sequelize";
// import connection 
import databaseAuth from "../config/databaseAuth.js";

const users = databaseAuth.define('users', {
  id: {
    primaryKey: true,
    type: Sequelize.STRING,
  },
  name: Sequelize.STRING,
  email: Sequelize.STRING,
  password: Sequelize.STRING,
  refresh_token: Sequelize.STRING
});

const log_auths = databaseAuth.define('log_auths', {
    id: {
      primaryKey: true,
      type: Sequelize.STRING,
    },
    auth_type: Sequelize.STRING,
    token: Sequelize.STRING,
    user_id: Sequelize.STRING
  });

export {
    users,
    log_auths
};