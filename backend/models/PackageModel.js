// import sequelize 
import { Sequelize } from "sequelize";
// import connection 
import databasePackage from "../config/databasePackage.js";

const products = databasePackage.define('products', {
  product_id: {
    primaryKey: true,
    type: Sequelize.STRING,
  },
  name: Sequelize.STRING,
  price: Sequelize.STRING,
  detail_features: Sequelize.STRING
});

export {
    products
};