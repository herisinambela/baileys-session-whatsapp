// import sequelize 
import { Sequelize } from "sequelize";
// import connection 
import databaseDevice from "../config/databaseDevice.js";

const devices = databaseDevice.define('devices', {
    device_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    name: Sequelize.STRING,
    telp: Sequelize.STRING,
    user_id: Sequelize.STRING,
    api_key: Sequelize.STRING,
    webhook:Sequelize.STRING,
    device_status:Sequelize.STRING,
    expired_at: Sequelize.STRING
});

export {
    devices
};