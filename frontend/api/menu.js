const Menu = [
  {header: 'Apps'},
  // {
  //   title: 'Dashboard',
  //   group: 'apps',
  //   icon: 'dashboard',
  //   name: 'dashboard',
  //   href: '/dashboard'
  // },
  {
    title: 'Device',
    group: 'apps',
    name: 'device',
    icon: 'perm_media',
    href: '/device/device'
  },
  {
    title: 'Subscribe',
    group: 'apps',
    name: 'subscribe',
    icon: 'perm_media',
    href: '/payment/subscribe'
  },
  // {
  //   title: 'Contact',
  //   group: 'apps',
  //   name: 'Contact',
  //   icon: 'perm_media',
  //   href: '/device/contact'
  // },
  // {
  //   title: 'Broadcast',
  //   group: 'apps',
  //   name: 'broadcast',
  //   icon: 'perm_media',
  //   href: '/device/broadcast'
  // },
  // {
  //   title: 'Chat',
  //   group: 'apps',
  //   icon: 'perm_media',
  //   target: '_blank',
  //   name: 'chat',
  //   href: '/device/chat/messaging'
  // }
];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
